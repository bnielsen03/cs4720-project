import java.io.*;
import java.net.Socket;

public class ChatClient implements Runnable {

   public static void main(String[] args) {
        if(args.length != 2) {
            System.out.println("Please specify host and port");
            System.out.println("java -jar build/libs/ChatClient.jar host port");
        }
        else {
            startClient(args[0], args[1]);
        }
    }

    private static void startClient(String host, String port) {
        try {
            Socket socket = new Socket(host, Integer.parseInt(port));
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream();

            Thread chatClient = new Thread(new ChatClient(inputStream));
            chatClient.start();

            PrintWriter printWriter = new PrintWriter(outputStream, true);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String input;
            while((input = bufferedReader.readLine()) != null) {
                printWriter.println(input);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final InputStream inputStream;
    ChatClient(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public void run() {
        try {
            this.inputStream.transferTo(System.out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
