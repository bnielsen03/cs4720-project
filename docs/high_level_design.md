# High level design

This document will explain the high level design in the ChatRoom application. 
For organizational purposes, the classes will be explained in the order that they
are ran.

## ChatClient.java

ChatClient.java is a class that is nearly identical to the ChatClient produced for assignment 10.
Most of the logic for this application is contained within the ChatServer. 
ChatClient.java contains a thread with a socket. The socket has inputs and outputs 
to send messages to the ChatServer and receive messages from the ChatServer.

## ChatServer.java

ChatServer.java creates a server socket at the specified port and creates
threads as sockets are joined. The thread started is the ChatRoomThread class. This class
contains an ArrayList of the ChatRoom class. An arraylist is used since there is a dynamic
number of ChatRooms that can be created.

## ChatRoom.java

The ChatRoom.java file contains a ChatRoom class. Each ChatRoom class contains an arraylist
of the Message class. It also contains the isPublic boolean and the password string. There
is also a wait object to wait() and notify from the JoinChatRoom class.

## Message.java

The Message class is each message created. This class holds the message string, 
the identifier of the user who created the message (to not notify the person who sent it),
the name of the sender, and the timestamp of when the message was sent.

## ChatRoomThread

ChatRoomThread is an inner class of ChatServer.java. ChatRoomThread is in charge of the
flow of the chatroom process. ChatRoomThread has inputs and outputs, the users name,
the user's unique identifier (to allow multiple user's with the same name), and the user's
joined chatroom. The ChatRoomThread is responsible for collecting the user's name, 
creating the unique identifier, and giving the user the option of joining an existing 
chatroom, or creating a new one.  If creating a new chatroom, this class instantiates the
CreateNewChatroom class. If joining an existing chatroom, this class instantiates the 
JoinExistingChatRoom class.  These two classes return a ChatRoom class. ChatRoomThread 
then instantiates the JoinChatRoom class and passes the ChatRoom class as a parameter.

## CreateNewChatroom

The CreateNewChatroom class is in charge of adding chatrooms to the chatRooms arraylist
that was created in ChatServer.java. This class prompts the user for the name of the 
chatroom, and if they would like to create a password.

## JoinExistingChatroom

The JoinExistingChatroom class displays all open chat rooms to the user as a menu.
The user is prompted to choose which chatroom to join. If the chatroom is private,
this class will prompt for a password until the correct password is given.

## JoinChatRoom

The JoinChatRoom class is the last class in the program. This class has producer/consumer
inner classes called Incoming and Outgoing. This class creates a ThreadPool and executes
the Incoming and Outgoing classes until they terminate.  The Incoming class is
responsible for waiting for input from the user. When input is given, it calls
a synchronous function on the ChatRoom class to add the message.  The Outgoing class
is responsible for displaying new messages to all users. 