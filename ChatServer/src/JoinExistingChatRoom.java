import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class JoinExistingChatRoom {
    private final PrintWriter printWriter;
    private final Scanner scanner;
    private final ArrayList<ChatRoom> chatRooms;

    JoinExistingChatRoom(PrintWriter printWriter, Scanner scanner, ArrayList<ChatRoom> chatRooms) {
        this.printWriter = printWriter;
        this.scanner = scanner;
        this.chatRooms = chatRooms;
    }

    public ChatRoom join() {
        String chatRoomIndex = null;

        while(chatRoomIndex == null) {
            printWriter.println("What chat room would you like to join (enter the number associated with the chatroom)?");

            int index = 0;
            for(ChatRoom room : chatRooms) {
                printWriter.println(index + ") " + room.getChatRoomName() + " (" + (room.getIsPublic() ? "public)" : "private)"));
                index++;
            }
            printWriter.flush();
            chatRoomIndex = scanner.nextLine();
        }

        ChatRoom chatRoom = chatRooms.get(Integer.parseInt(chatRoomIndex));

        boolean accessDenied = !chatRoom.getIsPublic();

        while(accessDenied) {
            printWriter.print("Please enter passcode: ");
            printWriter.flush();
            String pass = scanner.nextLine();

            accessDenied = !chatRoom.isPasswordCorrect(pass);
        }

        return chatRoom;
    }
}
