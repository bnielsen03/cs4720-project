import java.util.Date;

public class Message {
    private final String name;
    private final String identifier;
    private final String message;
    private final String time;
    public Message(String name, String identifier, String message) {
        this.name = name;
        this.identifier = identifier;
        this.message = message;
        this.time = new Date().toString().substring(11, 19);
    }

    public String getMessageIdentifier() {
        return identifier;
    }

    public String getMessage() {
        return "---" + time + "---\n" + name + ": " + message + "\n";
    }
}
