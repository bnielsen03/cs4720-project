import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class CreateNewChatroom {
    private final PrintWriter printWriter;
    private final Scanner scanner;
    private final ArrayList<ChatRoom> chatRooms;

    CreateNewChatroom(PrintWriter printWriter, Scanner scanner, ArrayList<ChatRoom> chatRooms) {
        this.printWriter = printWriter;
        this.scanner = scanner;
        this.chatRooms = chatRooms;
    }

    public ChatRoom createNewChatRoom() {
        String chatRoomName = getChatRoomName();
        String passCode = getChatRoomPasscode();
        ChatRoom chatRoom = new ChatRoom(chatRoomName, passCode);
        chatRooms.add(chatRoom);
        return chatRoom;
    }

    private String getChatRoomName() {
        String name = null;
        while(name == null) {
            printWriter.print("What would you like the name of the chatroom to be? ");
            printWriter.flush();
            name = scanner.nextLine();
        }
        printWriter.println("Chatroom is named " + name);
        printWriter.flush();

        return name;
    }

    private String getChatRoomPasscode() {
        String pass = null;
        while(pass == null) {
            printWriter.print("What is the passcode (leave blank for public room)? ");
            printWriter.flush();
            pass = scanner.nextLine();
        }

        return pass;
    }
}
