import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class ChatServer {
    public static ArrayList<ChatRoom> chatRooms = new ArrayList<>();

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Please specify port");
            System.out.println("java -jar build/libs/ChatClient.jar port");
        } else {
            startServer(Integer.parseInt(args[0]));
        }
    }

    private static void startServer(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);

            while (true) {
                Socket socket = serverSocket.accept();
                new ChatRoomThread(socket).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class ChatRoomThread extends Thread {
        private final Socket socket;
        private PrintWriter printWriter;
        private Scanner scanner;
        String name;
        ChatRoom chatRoom;
        String identifier;
        ChatRoomThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                while(true) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    scanner = new Scanner(socket.getInputStream());
                    printWriter = new PrintWriter(socket.getOutputStream());

                    if(name == null) {
                        name = getUserName();
                    }

                    if(identifier == null) {
                        identifier = Long.toHexString(Double.doubleToLongBits(Math.random()));
                    }

                    if(areWeGoingToCreateANewChatRoom()) {
                        chatRoom = (new CreateNewChatroom(printWriter, scanner, chatRooms)).createNewChatRoom();
                    }
                    else {
                        if(chatRooms.size() > 0) {
                            chatRoom = (new JoinExistingChatRoom(printWriter, scanner, chatRooms)).join();
                        }
                        else {
                            printWriter.println("There are currently no active chat rooms. Please create one.");
                            printWriter.println("");
                            printWriter.flush();
                            chatRoom = (new CreateNewChatroom(printWriter, scanner, chatRooms)).createNewChatRoom();
                        }
                    }

                    (new JoinChatRoom(name, identifier, chatRoom, printWriter, bufferedReader)).join();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private String getUserName() {
            String name = null;
            while(name == null) {
                printWriter.print("What is your name? ");
                printWriter.flush();
                name = scanner.nextLine();
            }
            printWriter.println("Your name is set to " + name);
            printWriter.flush();

            return name;
        }

        private boolean areWeGoingToCreateANewChatRoom() {
            String choice = "0";
            while(!choice.equals("1") && !choice.equals("2")) {
                printWriter.println("What would you like to do (enter option number)?");
                printWriter.println("\t 1) Join existing chatroom");
                printWriter.println("\t 2) Create new chatroom");
                printWriter.flush();
                choice = scanner.nextLine();
            }

            return choice.equals("2");
        }


    }
}
