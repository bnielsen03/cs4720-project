import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class JoinChatRoom {
    private final String name;
    private final ChatRoom chatRoom;
    private final String identifier;
    private final PrintWriter printWriter;
    private final BufferedReader bufferedReader;

    JoinChatRoom(String name, String identifier, ChatRoom chatRoom, PrintWriter printWriter, BufferedReader bufferedReader) {
        this.name = name;
        this.identifier = identifier;
        this.chatRoom = chatRoom;
        this.printWriter = printWriter;
        this.bufferedReader = bufferedReader;
    }

    public void join() {
        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        printWriter.println("Joining " + chatRoom.getChatRoomName());
        printWriter.flush();
        Runnable incoming = new Incoming();
        Runnable outgoing = new Outgoing();
        threadPool.execute(incoming);
        threadPool.execute(outgoing);

        try {
            threadPool.shutdown();

            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class Incoming extends Thread {
        @Override
        public void run() {
            try {
                String message;
                while ((message = bufferedReader.readLine()) != null) {
                    addMessage(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        synchronized private void addMessage(String message) {
            synchronized (chatRoom.wait) {
                chatRoom.addMessage(name, identifier, message);
                chatRoom.wait.notifyAll();
            }
        }
    }

    private class Outgoing extends Thread {
        @Override
        public void run() {
            int messagesPrinted = 0;
            while (true) {
                synchronized (chatRoom.wait) {
                    if (messagesPrinted != chatRoom.getMessageCount()) {
                        while (messagesPrinted < chatRoom.getMessageCount()) {
                            printWriter.println(chatRoom.getMessageExcludingUser(messagesPrinted, identifier));
                            messagesPrinted++;
                        }
                        printWriter.flush();
                    }
                    try {
                        chatRoom.wait.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
