import java.util.ArrayList;

public class ChatRoom {
    ArrayList<Message> messages = new ArrayList<>();
    String nameOfChatRoom;
    boolean isPublic = true;
    String password;
    public final Object wait = new Object();
    ChatRoom(String name, String password) {
        this.nameOfChatRoom = name;

        if(password != null && password.trim().length() > 0) {
            this.password = password;
            this.isPublic = false;
        }
    }

    public String getChatRoomName() {
        return nameOfChatRoom;
    }

    public boolean getIsPublic() {
        return isPublic;
    }

    public boolean isPasswordCorrect(String password) {
        return password.equals(this.password);
    }

    synchronized public void addMessage(String name, String identifier, String message) {
        this.messages.add(new Message(name, identifier, message));
    }

    public String getMessageExcludingUser(int index, String identifier) {
        Message message = messages.get(index);
        if(!message.getMessageIdentifier().equals(identifier)) {
            return message.getMessage();
        }

        return "";
    }

    synchronized public int getMessageCount() {
        return messages.size();
    }
}
