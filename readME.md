# Java Networking Chatroom
Java networking chatroom is a chatroom application that allows for public and private chatrooms.
Code can be found on [bitbucket here](https://bitbucket.org/bnielsen03/cs4720-project)
## Build Instructions

Each Folder (ChatClient and ChatServer) must be built with [gradle](https://docs.gradle.org/current/userguide/userguide.html).
This will create jar files.
If unpacking the zip file, there will be jar files already created.

```cd ChatClient # or cd ChatServer```
```gradle build```

## Usage

To run the application, run the jar file created by gradle.
You must first start the server before starting the clients.
Pass the required arguments.

First start the Server
``` 
cd ChatServer 
java -jar build/libs/ChatServer.jar 8080
```

Then start the Client

```
cd ChatClient
java -jar build/libs/ChatClient.jar 127.0.0.1 8080
```

## Platform Support

This program should work on all major OSes. It has been tested on Ubuntu 20.04.

## Similar Applications Consulted

Assignment 10 in CS4720 - ChatClient is nearly identical to submitted ChatClient.java in assignment 10.
The goal of my assignment was to allow users to connect to my ChatServer to use chatrooms and this file
was already perfect to allow me to expand the ChatRoom idea on the ChatServer.

[Chat Application Tutorial](https://cs.lmu.edu/~ray/notes/javanetexamples/#chat)

## Additional Documentation

The additional documentation (High-level design, screenshots) can be found in the `doc` directory.